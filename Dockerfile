FROM docker:19.03.5-dind

ENV TERRAFORM_VERSION=0.12.21

RUN apk add --no-cache \
    bash \
    curl \
    docker-compose \
    git \
    g++ \
    jq \
    make \
    nodejs \
    npm \
    python \
    python3 \
    unzip \
    zip

# Adding this to fix this message during pip3 install:
# You are using pip version 19.0.3, however version 19.1.1 is available. You
# should consider upgrading via the 'pip install --upgrade pip' command.
RUN pip3 install --no-cache-dir --upgrade pip

RUN pip3 install --no-cache-dir awscli

WORKDIR /usr/local/bin

RUN curl -o /tmp/terraform.zip "https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip" && unzip /tmp/terraform.zip && chmod +x /usr/local/bin/terraform && rm /tmp/terraform.zip

WORKDIR /root

RUN mkdir -p .ssh && echo "StrictHostKeyChecking no" > .ssh/config
RUN npm install -g serverless npm-check-updates yarn
